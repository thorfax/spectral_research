import json
import matplotlib.pyplot as plt
from sys import exit, argv
import numpy as np

if len(argv) < 2:
 print "missing input file"
 exit()

accs = {'Hall\'s Method': {'correct': 16, 'predicted': 2471}, 'Katz': {'correct': 1, 'predicted': 1002}, 'Common Neighbors': {'correct': 1, 'predicted': 1452}, 'Page Rank': {'correct': 8, 'predicted': 1002}, 'Shortest Path': {'correct': 275, 'predicted': 274057}, 'Preferential Attachment': {'correct': 0, 'predicted': 1002}}


names = accs.keys()
N = len(names)
accuracies = [100*float(accs[name]["correct"])/ accs[name]["predicted"] for name in names]
ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, accuracies, width, color='r')



# add some text for labels, title and axes ticks
plt.xticks(rotation=90)
ax.set_ylabel('Accuracy (%)')
ax.set_title('Accuracies by predictor.')
ax.set_xticks(ind + width)
ax.set_xticklabels(names)
fig.subplots_adjust(bottom=.4)
plt.show()


