import numpy as np
import graph_utils as gu
import argparse
import json
import networkx as nx

parser = argparse.ArgumentParser(description = "Make temporal graph from citation csv data.")
parser.add_argument("in_csv", type=argparse.FileType("r"))
parser.add_argument("out_json", type=argparse.FileType("w"))
parser.add_argument("max_authors", nargs="?", type=int, default=10000)

args = parser.parse_args()

names_to_num = {}
year_cliques = {}
def get_name_num(name):
  if name in names_to_num: return names_to_num[name]
  else:
    names_to_num[name] = len(names_to_num)
    return names_to_num[name]

def count_links(n):
  return n*(n-1)/2
  
max_edges = 0

for line in args.in_csv:
    line = line.strip().split(",")
    names = line[:-1]
    try:
      year = int(line[-1])
      if year not in year_cliques:
        year_cliques[year] = []
      #parse = False
      #if len(names_to_num) > args.max_authors:
	#res = []
        #for name in names:
	  #if name in names_to_num: res.append(names_to_num[name])
	#if len(res) >= 2: year_cliques[year].append(res)
      #else:
      year_cliques[year].append(map(get_name_num, names))
      max_edges += count_links(len(names))

    except:
      continue

def get_auth_frqs(year_cliques, nauths):
  counts = np.zeros(nauths, dtype=int)
  for y in year_cliques:
    for paper in year_cliques[y]:
      for auth in paper:
	counts[auth] += 1
  return np.argsort(counts)[::-1]	

def auth_filter(auths, year_cliques):
  res = {}
  for y in year_cliques:
    res[y] = []
    for paper in year_cliques[y]:
      c = []      
      for auth in paper:
	if auth in auths:
	  c.append(auth)
      if len(c) > 1: res[y].append(c)	  
  return res

nnodes = len(names_to_num)
auth_frqs = get_auth_frqs(year_cliques, nnodes)
auths =  set(auth_frqs[:min(nnodes, args.max_authors)])
#print "Maximum Edges %d, Nodes: %d" % (max_edges, len(names_to_num))
year_cliques = auth_filter(auths, year_cliques)
json.dump({"names_to_num":names_to_num, "auths": list(auths),"year_cliques":year_cliques}, args.out_json)