from BeautifulSoup import BeautifulSoup as bs
from sys import argv, exit
import networkx as nx
from graph_utils import to_mathematica_str

if len(argv) < 2:
	print "Usage infile"
	exit()

soup = bs(open(argv[1], "r").read())
for el in soup.findAll("tr"):
 code = el.findAll("td")[1].contents[0]
 G = nx.parse_graph6(code)
 print to_mathematica_str(G)
