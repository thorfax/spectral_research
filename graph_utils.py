import numpy as np
import scipy.linalg as la
import networkx as nx
from itertools import combinations
from matplotlib import pyplot as plt
from random import random
from networkx.readwrite import json_graph
import json

def add_random_edges(A, edges, cyclic=True):
 """
 INPUTS:
 A -- The adjacency matrix for a graph
 edges -- The number of random edges to add
 cycles -- Whether the graph is allowed to be cyclic. If set to False, no edges will be added that make the graph contain a cycle.

 RETURNS:
 Nothing, A is modified in place
 """
 pass

def random_preferential_attach(nnodes, node_edges):
 """
 Create a preferential attachement with node_edges
 """
 G = nx.Graph()
 G.add_nodes_from(range(nnodes))
 for node in xrange(nnodes):
   if not node:
     continue
   neighbors = np.random.choice(node, min(node_edges,node),replace=False)
   for n in neighbors:
    G.add_edge(node, n)
 return G

def maybe_add_edge(G, node1, node2, attach_distribution):
   d1, d2 = len(G.edge[node1]), len(G.edge[node2])
   u = random()
   if u < attach_distribution(d1)*attach_distribution(d2):
     G.add_edge(node1, node2)

def to_mathematica_str(G):
  node_list = G.nodes()
  rev_dict = {node_list[i] : i+1 for i in xrange(len(node_list))}
  return "{"+",".join(map( lambda e: "{"+",".join(map(lambda n: str(rev_dict[n]),e))+"}",G.edges()))+"}"

def modified_preferential_attach(nnodes, attach_distribution, node_contacts):
 G = nx.Graph()
 G.add_nodes_from(range(nnodes))
 for node in xrange(nnodes):
   if not node:
     continue
   contacted_neighbors = np.random.choice(node, min(node_contacts,node),replace=False)
   for n in contacted_neighbors:
    maybe_add_edge(G, node, n, attach_distribution)
 return G

def biggest_component(g):
    """
    Return the biggest connected component of a graph g
    """
    msize = 0
    best_g = None
    for c in nx.connected_component_subgraphs(g):
        nnodes = len(c.nodes())
        if nnodes > msize:
            msize = nnodes
            best_g = c
    return best_g 
    

def plot_graph_degree_dist(G, bins=None):
 nodes = G.nodes()
 if bins is None:
   bins = max(nodes)-min(nodes)
 edge_lens=[len(G.edge[node]) for node in nodes]
 plt.hist(edge_lens, bins=bins)
 plt.show()
 
def limited_resource_graph(nnodes, attach_distribution, p=.5):
 """
 Create a random graph on nnodes according to the following procedure:
 The idea is that nodes have limited capacity, and a link between two nodes forms with probability f(d1)*f(d2) 
 p is an upper sparsity bound
 """
 G = nx.Graph()
 G.add_nodes_from(range(nnodes))
 ntrials = int(p*nnodes)
 for i in xrange(ntrials):
   node1, node2 = np.random.choice(nnodes, 2, replace=False)
   maybe_add_edge(G, node1, node2, attach_distribution)
 return G

def random_connected_graph(nodes, edges):
 """
 INPUTS:
 nodes -- The number of nodes desired in the graph
 edges -- The desired number of edges

 DESCRIPTION:


 RETURNS:
 A -- The adjacency matrix for the graph
 """

 if edges < nodes-1:
  raise ValueError("The number of edges must be at least nodes-1 for the graph to be connected.")

 #Initialize adjacency matrix
 A = np.zeros((nodes,nodes))

 for i in xrange(1,nodes):
  #pick a random node from the visited nodes
  n = np.random.randint(i)

  #add the edge n-i to the graph
  A[i,n] = 1.
  A[n,i] = 1.

 #At this point, there are nodes-1 edges in the graph, so we need to add more
 add_random_edges(A,edges-nodes+1)

 return A

#A helper function to see if two sets of predicted links are acutally the same
#Used to verify two algorithm implementations are actually the same
def equal_preds(l1, l2):
  s1 = set()
  s2 = set()
  for el in l1:
    s1.add(el)
    s1.add((el[1], el[0]))

  for el in l2:
    s2.add(el)
    s2.add((el[1], el[0]))
  return s1 == s2

def star_adj(n):
 A = np.zeros((n,n))
 A[0,1:] = np.ones(n-1)
 A[1:,0] = np.ones(n-1)
 return A

def laplacian_from_adj(A):
 a = np.zeros(A.shape[0])
 a[:] = np.sum(A,axis=0)
 D = np.diag(a)
 return D-A

def fiedler_eigenspace(laplacian, tol=1e-14):
 evals, evecs = la.eigh(laplacian)
 #get feidler eigen-value
 feidler_eval = evals[np.argsort(evals)[1]]
 #Figure out which eigen-vectors correspond to the feidler eigen-value
 feidler_inds = np.where(np.abs(evals-feidler_eval) < tol)
 #Extract these eigen-vectors and orthogonalize them, just in case
 node_coords = evecs.T[feidler_inds]
 node_coords = la.orth(node_coords.T)
 return node_coords

def top_nodes(G, fraction = .2):
    """
    Extract the fraction*nnodes nodes of biggest degree from G
    """
    nnodes = len(G)
    required_nodes = int(fraction * nnodes)    
    degrees = [(-len(G[x]), x) for x in G]
    reduced = sorted(degrees)[:required_nodes]
    reduced = [r[1] for r in reduced]
    return reduced

def project_graph(G, node_list):
    new = nx.Graph()
    node_set = set(node_list)
    for x in node_set:
        if x not in G: continue
        for y in G[x]:
            if y in node_set:
                new.add_edge(x,y)
    return new

def smallest_laplacian_eigenspaces(laplacian, num_spaces = 1, force_dimension = None, tol=1e-10, return_evals = False, sparse_opt=False, **kwargs):
 """
 When num_spaces = 1, this is the same as fiedler_eigenspace.
 Otherwise, returns an n x k matrix V, where n is the dimension of laplacian
 V is a concatenation of the basis for the first num_spaces spaces of the laplacian.
 If there are fewer distinct eigenvalues than num_spaces, all are returned.
 If force_dimension is set, then the output will be guaranteed to have exactly force_dimensions dimensions.
 """
 if sparse_opt:
   #TODO use a more optimized solver here
   evals, evecs  = la.eigh(laplacian)
 else: evals, evecs = la.eigh(laplacian)
 inds = np.argsort(evals)
 spaces = 0
 i = 1 #skip the first zero eigenvalue
 n = evals.size
 evals = np.sort(evals)
 res = None

 if force_dimension is not None:
   #Don't worry about the number of eigenspaces
   num_spaces = n


 while i < n and spaces < num_spaces:
  curr_eval = evals[i]
  spaces += 1
  if i == 1:
    res = evecs[:,i].reshape(n,1)
  else:
    res = np.concatenate((res,evecs[:,i].reshape(n,1)),axis=1)
  while i < n:
    i+=1
    if i < n and abs(evals[i]-curr_eval) < tol:
      res = np.concatenate((res,evecs[:,i].reshape(n,1)),axis=1)
    else:
      break

  if force_dimension is not None:
    current_dimensions = res.shape[1]
    if force_dimension <= current_dimensions:
      if not force_dimension == current_dimensions:
         #We need to project the space down onto the correct number of dimensions (using the svd)
         #This could be because we need to plot it two-dimensionally
         print "Projecting"
         res = project_space(res, force_dimension)
         break
      else: break

 if return_evals:
   return res, evals[1:res.shape[1]+1]
 return res


def graph_laplacian(G):
 A = nx.to_numpy_matrix(G)
 return np.diagflat(np.sum(A, axis = 0)) - A

def draw_spectral(laplacian,**kwargs):
 node_coords = smallest_laplacian_eigenspaces(laplacian, **kwargs)
 intelligent_scatter(node_coords)

#IDEA: What if I project the Euclidean representation of the graph onto a lower space using the svd?
#What other ways are there to project and preserve features?
def project_space(space, output_dimensions):
 """
 Project the given space onto one of dimension output_dimension, using the SVD.
 """
 U, s, V = la.svd(space)
 if output_dimensions > s.size:
  raise ValueError("Cannot project a space of dimension "+str(space.size)+" onto one of size "+str(output_dimensions))
 s = s[:output_dimensions]
 U = U[:, :output_dimensions]
 for i in xrange(output_dimensions):
  U[:,i] = U[:,i] * s[i]
 return U

def intelligent_scatter(node_coords,**kwargs):
 """
 Scatter points in node_coords representing the nodes of a graph.
 Dynamically detects dimension and plots accordingly.
 """
 num_points, dimension  = node_coords.shape
 if dimension > 3:
  raise ValueError("Eigenspace has dimension > 3, plotting not possible.")
 elif dimension == 3:
  plt.scatter(node_coords[:,0],node_coords[:,1],node_coords[:,2])
 elif dimension == 2:
  plt.scatter(node_coords[:,0],node_coords[:,1])
 else:
  plt.scatter(node_coords[:,0],np.zeros(num_points))
 plt.show()


def gen_martin_graph(edges, n):
  G = nx.Graph()
  s1 = np.arange(2*edges)
  s2 = np.arange(2*edges, 2*edges +n)
#  2*edges

def hack_gml_read(fname):
  g = nx.Graph()
  with open(fname, "r") as f:
    lsource = None
    for line in f:
      arr = line.strip().split(" ")
      if not len(arr): continue
      if arr[0] == "source":
        lsource = int(arr[1])
      elif arr[0] == "target":
        g.add_edge(lsource, int(arr[1]))
  return g

def gml_with_labels(fname):
    g = nx.Graph()
    labels = {}
    with open(fname, "r") as f:
        lsource, last_id = None, None
        for line in f:
            arr = line.strip().split(" ")            
            if arr[0] == "id":
                last_id = int(arr[1])
            elif arr[0] == "label":
                labels[last_id] = line.split('"')[1]
            elif arr[0] == "source":
                lsource = int(arr[1])
            elif arr[0] == "target":
                g.add_edge(lsource, int(arr[1]))
    return g, labels

def write_json_graph(graph, labels, outfile):
    json.dump({'graph':json_graph.adjacency_data(graph), 'labels':labels}, outfile)

def read_json_graph(infile):
    d = json.load(open(infile,"r"))
    return json_graph.adjacency_graph(d["graph"]), d["labels"]

def general_graph_read(filename):
    if filename.endswith("net"):
        return save_pajek_read(filename)
    elif filename.endswith("gml"):
        return hack_gml_read(filename)

def save_graph_list(l, filename):
 """
 INPUTS:
 l -- the list of graphs to be saved
 filename -- the file to save the graphs to
 DESCRIPTION:
 Saves the list l of networkx graphs to file filename
 RETURNS:
 None
 """
 pass

def load_graph_list(filename):
 """
 INPUTS:
 filename -- the file to save the graphs to
 DESCRIPTION:
 Loads a saved list of graphs from a file.
 RETURNS:
 l -- the list of graphs
 """
 pass

def martin_graph(candidates, categories):
    G = nx.Graph()
    nodes = np.arange(candidates, dtype = int)
    variances = np.arange(candidates/3)
    for i in xrange(categories):
        #partition the candidates into two partitions
        var = np.random.choice(variances)
        num = candidates/2 + var
        first = np.random.choice(nodes, num, replace = False)
        second = np.setdiff1d(nodes, first)
        first_label = candidates + 2*i
        second_label = first_label + 1
        for el in first:
            G.add_edge(first_label, el)
        for el in second:
            G.add_edge(second_label, el)
    return G

from time import time

def do_graph_experiment(train, test, pred_dict, min_links=None, new_edge_frac=.1):
    result = {}
    projected_test = project_graph(test, train.nodes())
    test_edges = projected_test.number_of_edges()
    train_edges = train.number_of_edges()
    num_new_edges = test_edges - train_edges
    if min_links is None: min_links = int(num_new_edges*new_edge_frac)
    nnodes = train.number_of_nodes() 
    result["info"] = {"train_info":nx.info(train), "random_ratio": float(num_new_edges) / (nnodes*(nnodes-1)/2 -train_edges), "min_links":min_links, "new_edge_frac":new_edge_frac}
    print result
    for name in pred_dict:
        try:
            constructor = pred_dict[name]
            start = time()
            predictor = constructor(train)
            nlinks, ncorrect = predictor.validate(test, min_links=min_links)
            elapsed = time() - start
            result[name] = {"time":elapsed, "nlinks":nlinks, "ncorrect":ncorrect, "ratio":float(ncorrect)/nlinks}
        except (ValueError, NameError, TypeError) as e:
            print e
            continue
    return result    
def print_graph_size(G):
 print "Nodes:"+str(len(G.nodes())), "Edges: " + str(len(G.edges()))
