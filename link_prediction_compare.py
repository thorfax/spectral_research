import networkx as nx
from sys import argv, exit
import base_predictor
import other_predictors
from spectralPredictor import spectralPredictor
from weightedSpectral import *
import numpy as np
import matplotlib.pyplot as plt
from graph_utils import print_graph_size
import json

usage = "training filename test filename output file (for accuracies)"
argc = len(argv)
if argc < 3:
  print usage
  exit()
else:
  train_fname = argv[1]
  test_fname = argv[2]

def hack_gml_read(fname):
  g = nx.Graph()
  with open(fname, "r") as f:
    lsource = None
    for line in f:
      arr = line.strip().split(" ")
      if not len(arr): continue
      if arr[0] == "source":
        lsource = int(arr[1])
      elif arr[0] == "target":
        g.add_edge(lsource, int(arr[1]))
  return g


predictors = {}
#predictors["Katz"] = other_predictors.katz
#predictors["Common Neighbors"] = other_predictors.commonNeighbors
#predictors["Shortest Path"]= other_predictors.shortestPath
#predictors["Page Rank"] = other_predictors.pageRank
#predictors["Jaccard"] = other_predictors.jaccard
#predictors["Preferential Attachment"] = other_predictors.preferentialAttachment
#predictors["Adamic Adar"] = other_predictors.AdamicAdar
#predictors["Resource Allocation"] = other_predictors.resourceAllocation
#predictors["Base"] = base_predictor.basePredictor
#predictors["Spectral"] = spectralPredictor
predictors["Weighted Spectral"] = weightedSpectral

train = hack_gml_read(train_fname)

print "Training graph: "
print_graph_size(train)

test = hack_gml_read(test_fname)

print "Testing graph: "
print_graph_size(test)


total_accuracies = {name: {"correct":0, "predicted":0} for name in predictors}


for comp in nx.connected_component_subgraphs(train):
  if len(comp.nodes()) < 100:
    continue
  print_graph_size(comp)
  for name in predictors:
    print "On "+name
    predictor = predictors[name](comp)
#    predictor.setReadCache(name+"_"+train_fname)
    predictor.setWriteCache(name+"_"+train_fname)
    npredicted, ncorrect = predictor.validate(test, min_links = 1000)
    total_accuracies[name]["predicted"] += npredicted
    total_accuracies[name]["correct"] += ncorrect
    print npredicted, ncorrect

names = predictors.keys()
N = len(names)
accuracies = [100*float(total_accuracies[name]["correct"])/ total_accuracies[name]["predicted"] for name in names]
ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, accuracies, width, color='r')

plt.xticks(rotation=90)
ax.set_ylabel('Accuracy')
ax.set_title('Accuracies by predictor.')
ax.set_xticks(ind + width)
ax.set_xticklabels(names)
plt.show()
print total_accuracies
if argc >= 4:
  ofname = argv[3]
  json.dump(total_accuracies, open(ofname,"w"))

