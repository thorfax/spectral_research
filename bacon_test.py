from link_predictor import *
from itertools import combinations
import other_predictors
import base_predictor
import networkx as nx
import matplotlib.pyplot as plt

def parse(filename="movieData.txt", cutoff_movies = 3, cutoff_weight = 3):
    """
    Return a list of movies and actors
    Create an adjacency graph where two actors are connected if they appear in the same movie
    """

    # open the file, read it in, and split the text by '\n'
    movieFile = open(filename, 'r')
    movieFile = movieFile.read()
    movieFile = movieFile.split('\n')
    G = nx.Graph()
    actor_movies = {} #a dictionary mapping actors to movies
    # for each movie in the file,
    for line in movieFile:
        # get movie name and list of actors
        names = line.split('/')
        movie = names[0]
        # add the actors to the dictionary
        for actor1, actor2 in combinations(names[1:],2):
            if G.has_edge(actor1, actor2):
             G[actor1][actor2]['weight'] += 1
            else:
             G.add_edge(actor1, actor2,weight = 1)
        for actor in names[1:]:
            if actor not in actor_movies:
               actor_movies[actor] = [movie]
            else:
               actor_movies[actor].append(movie)
    
    
    
    for actor in actor_movies:
      #only keep actors who have been in a decent number of movies
      if len(actor_movies[actor]) < cutoff_movies:
        try: 
          G.remove_node(actor)
        except:
          continue
    return G

def edge_filter(G, cutoff_weight = 3,weights = False):
 """
 Create a filtered version of G only containing edges of weight greater than cutoff_weight
 """
 result = nx.Graph()
 for node1 in G:
  for node2 in G[node1]:  
   if G[node1][node2]['weight'] >= cutoff_weight:
    if weights:
      result.add_edge(node1, node2, weight = G[node1][node2]['weight'])
    else: 
      result.add_edge(node1, node2)
 
 return result
 
def predict_and_validate(true_graph, partial_graph):
  """
  Returns total_pred, ncorrect
  TODO: Plot prediction accuracy as a function of the number of spaces used.
  """
  l = linkPredictor(partial_graph)
  links = l.predictLinks(num_spaces=12)
  ncorrect = 0
  for link in links:
    if true_graph.has_edge(link[0], link[1]):
     ncorrect += 1
  return len(links), ncorrect

def print_graph_size(G):
 print "Nodes:"+str(len(G.nodes())), "Edges: " + str(len(G.edges()))

predictors = {"Katz": other_predictors.katz, "Common Neighbors": other_predictors.commonNeighbors, "Shortest Path": other_predictors.shortestPath}
predictors["Page Rank"] = other_predictors.pageRank
predictors["Jacard"] = other_predictors.Jacard
predictors["Preferential Attachment"] = other_predictors.preferentialAttachment
predictors["Base"] = base_predictor.basePredictor

orig = parse()
nx.draw(orig)
plt.show()
print_graph_size(orig)
g = edge_filter(orig, cutoff_weight = 3)
nx.draw(g)
plt.show()
print_graph_size(g)

total_accuracies = {name: {"correct":0, "links":0} for name in predictors}

tot_links = 0
tot_correct = 0

for sg in nx.connected_component_subgraphs(g):
 nnodes = len(sg.nodes())
 nedges = len(sg.edges())
 if nnodes > 3:
  print "Nodes %d, Edges %d " % (nnodes, nedges)
  num_links, ncorrect = predict_and_validate(orig, sg)
  for name in predictors:
    predictor = predictors[name](sg)
    tlinks, tcor = predictor.validate(orig, min_links = num_links)
    total_accuracies[name]["links"] += tlinks
    total_accuracies[name]["correct"] += tcor
  tot_links += num_links
  tot_correct += ncorrect
  print "Links %d, Correct %d" % (num_links, ncorrect)


for name in predictors:
  cor, links = total_accuracies[name]["correct"], total_accuracies[name]["links"]
  print name, " got overall accuracy of "+ str( 100*float(cor)/links) + "% links: ", links
print ("Extended Hall's Method got overall accuracy: %f" % (100*float(tot_correct) / float(tot_links))) + "% links: ", tot_links 

N = len(predictors)+1
names = predictors.keys()
accuracies = [100*float(total_accuracies[name]["correct"])/ total_accuracies[name]["links"] for name in names]
accuracies.append(100*float(tot_correct) / float(tot_links))
ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, accuracies, width, color='r')



# add some text for labels, title and axes ticks
plt.xticks(rotation=90)
ax.set_ylabel('Accuracy')
ax.set_title('Accuracies by predictor.')
ax.set_xticks(ind + width)
ax.set_xticklabels(names+["Hall's Method"])

fig.subplots_adjust(bottom=.4)

plt.show()


#res = [('Deville, Kurt', 'Lord, Mark F.'), ('Crews, Terry', 'Palmer, Teresa'), ('Smith, Terry Lee', 'Sarandon, Susan'), ('Keach, Stacy', 'Mulvey, Callan'), ('Jones, Julia', 'Carney, D.T.'), ('Cudlitz, Michael', 'Bowen, Michael'), ('Williams, Katt', 'Quinn, Aidan'), ('Gasteyer, Ana', 'Bubar, Pablo'), ('Shea, Dan', 'Cruise, Tom'), ('Sadler, William', 'Hines, Garrett'), ('Hawkes, John', 'Campbell, Christa'), ('Rumsey, Tyler', 'Moore, Jesse'), ('Hendrix III, Walter', 'Sheen, Martin'), ('Chen, James', 'Spence, Rebecca'), ('McConaughey, Matthew', 'Graves, Emilia'), ('Distance, Brian', 'Dennehy, Ned'), ("D'Leo, John", 'Anderson, Joe'), ('Bernthal, Jon', 'Barlow Jr., Paul'), ('Peterson, Alan C.', 'Perlman, Ron'), ('Wolejnio, Katarzyna', 'Marquette, Chris'), ('Morris, Jon L.', 'Biel, Jessica'), ('Martin, Demetri', 'Kelly, Colleen'), ('Sry, Kelly', 'Nicholson, Scott'), ('McGinley, John C.', 'Lombardi, Louis'), ('Kapor, Milorad', 'Noonan, Blaire'), ('Shreves, Catherine', 'Ifans, Rhys'), ('Hebert, Rodney', 'Lillard, Matthew'), ('Ortiz, Gustavo I.', 'Zayats, Andrei'), ('Hoogenakker, John', 'Adams, Amy'), ('Sabongui, Patrick', 'Bening, Annette'), ('Lloyd-Hughes, Henry', 'Lewis, Lucinda'), ('Landry, Jack', 'Keiffer, Dennis'), ('Blake, Dalias', 'Pearl, Aaron'), ('Gleeson, Domhnall', 'Johansson, Scarlett')]
#for n1, n2 in res:
# print nx.shortest_path_length(g, source = n1, target = n2)
#l = linkPredictor(g)
#print l.predictLinks()

