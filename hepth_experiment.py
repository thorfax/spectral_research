import networkx as nx
import numpy as np
import graph_utils as gu
import numpy.linalg as la
import time
import matplotlib.pyplot as plt
import eigensolvers as es
import base_predictor
import other_predictors as op
from spectral_embedding import SpectralEmbedding as se, cosinePredictor
from functools import partial
import json

def parse_collaboration_graph(datafile):
    temporal_edges = []
    max_i = 1000
    i=0
    with open(datafile, "r") as f:
        for line in f:
            if line[0] == "%": continue
            i += 1
            ar = line.strip().split()
            edge = (int(ar[0]), int(ar[1]))
            timestamp = int(ar[-1])
            temporal_edges.append([timestamp, edge])
    return sorted(temporal_edges)

def do_colab_graph_experiment(infile, outfile):
    tedges = parse_collaboration_graph(infile)
    cutoff = 3*len(tedges)/4
    edges = [t[1] for t in tedges]
    train = nx.Graph()
    test = nx.Graph()
    train.add_edges_from(edges[:cutoff])
    test.add_edges_from(edges)
    train = gu.biggest_component(train)
    tn = gu.top_nodes(train, fraction = .1)
    train_small = gu.biggest_component(gu.project_graph(train, tn))
    test_small = gu.project_graph(test, tn)
    print nx.info(train_small)
    print nx.info(test_small)

#all_pred = {"katz":op.katz, "commonneighbors":op.fastCommonNeighbors, "prefattach":op.preferentialAttachment}
    all_pred = {"pageRank":op.pageRank,"adamic":op.AdamicAdar}
    spred = {}
#dims = [1,2,4,8,16]
#for dim in dims:
#    spred["spec_euclid"+str(dim)] = partial(se, dim=dim)
#    spred["spec_cosine"+str(dim)] = partial(cosinePredictor, dim=dim)
#for pred in spred: all_pred[pred] = spred[pred]
    res = gu.do_graph_experiment(train_small, test_small, all_pred)
    print res
    json.dump(res,open(outfile, "w"))
#spec_pred = {"prefattach":op.fastPreferentialAttachment}
#dims = [1,2,4,8,16]
#for dim in dims:
#    spec_pred["spec_euclid"+str(dim)] = partial(se, dim=dim)
#    spec_pred["spec_cosine"+str(dim)] = partial(cosinePredictor, dim=dim)
#res = gu.do_graph_experiment(train, test, spec_pred, min_links=1000)
#print res
#json.dump(res,open("data/hepth_result.json", "w"))

do_colab_graph_experiment("data/ca-cit-HepPh/out.ca-cit-HepPh", "data/hepph_small_adamic_result.json")
