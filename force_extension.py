import json
import argparse
import graph_utils as gu
from networkx.readwrite import json_graph
import networkx as nx

def fix_keys(d):
    new = {}
    for k in d:
        new[int(k)] = d[k]
    return new

p = argparse.ArgumentParser(description='Given two graphs, A and B, with labels for the nodes of A and the nodes of B, relabel the nodes of B to match up with the nodes of A.')
p.add_argument('base_graph', type=argparse.FileType('r'))
p.add_argument('extension_graph', type=argparse.FileType('r'))
p.add_argument('outfile', type=argparse.FileType('w'))
args = p.parse_args()

base = json.load(args.base_graph)
ext = json.load(args.extension_graph)
bg = json_graph.adjacency_graph(base['graph'])
bl = fix_keys(base['labels'])

eg = json_graph.adjacency_graph(ext['graph'])
el = fix_keys(ext['labels'])

bd = {bl[node]:node for node in bl}
ed = {el[node]:node for node in el}

new_g = nx.Graph()
for e1, e2 in eg.edges():    
    l1, l2 = el[e1], el[e2]
    if l1 in bd and l2 in bd:
        new_g.add_edge(bd[l1], bd[l2])

gu.write_json_graph(new_g, bl, args.outfile)

