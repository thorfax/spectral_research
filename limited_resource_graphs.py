from graph_utils import *
import networkx as nx
import matplotlib.pyplot as plt

N = 200
sparsity = 20
attach_func = lambda degree: 1./(1+degree)
for i in xrange(5):
# G = limited_resource_graph(N,attach_func, sparsity)
 #plt.title("Average Shortest Path Lengths: %f"%nx.average_shortest_path_length(G))
 G=random_preferential_attach(N,sparsity )
 plt.title("Pref Attach")
 plot_graph_degree_dist(G,bins=10)
 plt.title("Modified Pref attach")
 G=modified_preferential_attach(N, attach_func,sparsity )
# nx.draw(G)
 #plt.show()
 plot_graph_degree_dist(G,bins=20)
