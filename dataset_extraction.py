#A single file with all dataset extraction code
import networkx as nx
import graph_utils as gu
import json

#A class that stores a train, test pair
class testCase:
    def __init__(self, train=None, test=None):
        self.train = gu.biggest_component(train)
        self.test = test

def parse_collaboration_graph(datafile):
    temporal_edges = []
    with open(datafile, "r") as f:
        for line in f:
            if line[0] == "%": continue
            ar = line.strip().split()
            edge = (int(ar[0]), int(ar[1]))
            timestamp = int(ar[-1])
            temporal_edges.append([timestamp, edge])
    return sorted(temporal_edges)

def extract_collab_graph(infile):
    tedges = parse_collaboration_graph(infile)
    cutoff = 3*len(tedges)/4
    edges = [t[1] for t in tedges]
    train = nx.Graph()
    test = nx.Graph()
    train.add_edges_from(edges[:cutoff])
    test.add_edges_from(edges)
    big = testCase(train=train, test=test)
    tn = gu.top_nodes(train, fraction = .1)
    small = testCase(train = gu.project_graph(train, tn), test = gu.project_graph(test, tn))
    return big, small

def parse_facebook_graph(datafile = "data/facebook-links.txt"):
    temporal_edges = []
    other_edges = []
    with open(datafile, "r") as f:
        for line in f:
            ar = line.strip().split()
            edge = (int(ar[0]), int(ar[1]))
            if 'N' in ar[-1]:
                other_edges.append(edge)
            else:
                timestamp = int(ar[-1])
                temporal_edges.append([timestamp, edge])
    return sorted(temporal_edges), other_edges

def extract_hepph():
    return extract_collab_graph("data/ca-cit-HepPh/out.ca-cit-HepPh")

def extract_hepth():
    return extract_collab_graph("data/ca-cit-HepTh/out.ca-cit-HepTh")

def extract_facebook():
    tedges, oedges = parse_facebook_graph()
    times = [t[0] for t in tedges]
    temporal_edges = [t[1] for t in tedges]
    train = nx.Graph()
    test = nx.Graph()
    cutoff = 3*len(times)/4
    train.add_edges_from(oedges+temporal_edges[:cutoff])
    test.add_edges_from(oedges+temporal_edges)
    return testCase(train=train, test=test)

def extract_cond_mat():
    train = gu.read_json_graph("data/cond-mat.json")[0]
    test = gu.read_json_graph("data/cond-mat-2003-fixed.json")[0]
    train = gu.biggest_component(train)
    return testCase(train=train, test=test)

extractors = {"facebook":extract_facebook, "hepph":extract_hepph, "hepth":extract_hepth,
    "cond-mat":extract_cond_mat}
    
network_names = extractors.keys()
def extract_network(name):
    return extractors[name]()

