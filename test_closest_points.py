import numpy as np
from closest_points import *
import time
import matplotlib.pyplot as plt

def repeated_test(iters=100, n=50, d=2, k=2):
    outfile = "bad.npz"
    for i in xrange(iters):
        a = np.random.random((n,d))
        a = a.reshape((n,d))
        print "Tested {} times.".format(i+1)
        if not test_eq(a,k):
            np.savez(outfile, a=a, k=np.array([k]))
            raise Exception("The algorithm broke!!!!")
        

def test_eq(a, k=2):
    inds, dists = closest_points(a,k=k)
    true_inds, true_dists = brute_closest_points(a, k=k)
    return np.all(inds == true_inds)

def from_file(name = "bad.npz"):
    ark = np.load(name)
    a = ark["a"]
#    print a
    k = ark["k"][0]
    print k
    inds, dists = closest_points(a,k=k)
    true_inds, true_dists = brute_closest_points(a, k=k)
    print true_inds[-1], true_dists[-1]
    print inds, true_inds    
    print inds[-1], dists[-1]

def compare_times(n,d,k):
    """
    Return: t1, t2
    t1 -- time for slow algorithm
    t2 -- time for fast algorithm
    """
    a = np.random.random((n,d))
    start1 = time.time()
    inds, dists = brute_closest_points(a, k)
    t1 = time.time()-start1
    start2 = time.time()
    inds, dists = closest_points(a,k)
    t2 = time.time()-start2
    return t1, t2

def time_algo(n, d, k):
    a = np.random.random((n,d))
    start = time.time()
    inds, dists = closest_points(a,k)
    t = time.time()-start
    return t


def plot_times_by_n(n_arr, d = 2, k=2, compare=True):
    brute_times = []
    fast_times = []
    if compare:
        for n in n_arr:
            t1, t2 = compare_times(n, d, k)
            brute_times.append(t1)
            fast_times.append(t2)
    
        plt.plot(n_arr, brute_times, label = "Brute")
        
    else:
        for n in n_arr:
            fast_times.append(time_algo(n,d,k))
            
        
    plt.plot(n_arr, fast_times, label = "Fast")    
    plt.xlabel("n")
    plt.ylabel("time")
    plt.legend(loc='upper left')
    plt.show()
        
    
if __name__ == "__main__":
    for k in xrange(1,200):
        repeated_test(k=k)
#    from_file()
 #  n_arr = [2**i for i in xrange(4,20)]
#   plot_times_by_n(n_arr, compare=False)
    
