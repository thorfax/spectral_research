import networkx as nx
import graph_utils as gu
import numpy as np

def hack_gml_read(fname):
  g = nx.Graph()
  with open(fname, "r") as f:
    lsource = None
    for line in f:
      arr = line.strip().split(" ")
      if not len(arr): continue
      if arr[0] == "source":
        lsource = int(arr[1])
      elif arr[0] == "target":
        g.add_edge(lsource, int(arr[1]))
  return g


#print gu.gml_with_labels("cond-mat.gml")
