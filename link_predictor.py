from graph_utils import *
from itertools import combinations

class linkPredictor:
  """
  INPUTS: G -- for now a networkx graph
  I will possibly support other graph representations in the future, if needed.
  """
  
  def __init__(self,G):
    self.G = G
    self.node_list = G.nodes()
    #I want to be able to reuse this if it has already been calculated
    #However, certain link prediction methods may not need the fiedler vectors
    #Others might compute it in special ways. Consequently, I'm not initializing it right off the bat.
    self.eigenspace = None
    
  
  def predictLinks(self,algorithm="minimum_distance", return_weights=False,**kwargs):
    """
    INPUTS:
    algorithm (optional) -- the choice of algorithm to use
    """
    if self.eigenspace is None or len(kwargs) > 0:
     self.init_eigenspace(**kwargs)
    self.make_weightlist(self.eigenspace)

    
    if algorithm == "minimum_distance":
      #Dr Barett's algorithm 
      return self.minimum_distance_links(return_weights)
#    elif algorithm == "eval_radius":
 #     return self.eva
    else: 
      raise ValueError("Algorithm "+algorithm+" currently not implemented.")
  
  #Dr Barett's algorithm for link prediction using the minimum distance between nodes
  def minimum_distance_links(self,return_weights=False,tol=1e-10):
    weights = self.weights
    #Nothing to predict, as this means a complete graph
    if len(weights) == 0:
     return []
    min_distance = min(weights.values())    
    edge_list = []
    
    for edge in weights:
      if abs(weights[edge]-min_distance) < tol:
        if return_weights == True:
         edge_list.append((edge[0], edge[1], weights[edge]))
        else:
         edge_list.append(edge)
    return edge_list
     
  def draw_spectral(self, **kwargs):
    if self.eigenspace is None or len(kwargs) > 0:
     self.init_eigenspace(**kwargs)
    
    #TODO use a better drawing scheme
    
    intelligent_scatter(self.eigenspace,**kwargs)

  def draw_2d(self, d1=False):
     L = graph_laplacian(self.G)
     self.eigenspace = smallest_laplacian_eigenspaces(L, force_dimension=2)
     rev_dict = {l:i for i,l in enumerate(self.node_list)}
     if d1: self.eigenspace[:,1] = 0
     nx.draw_networkx(self.G, {i: self.eigenspace[rev_dict[i],:] for i in self.node_list})
     plt.show()
  
  def draw_nx(self):
   nx.draw(self.G)
   plt.show()
   
  def init_eigenspace(self, **kwargs):
     L = graph_laplacian(self.G)
     self.eigenspace = smallest_laplacian_eigenspaces(L, **kwargs)
     
  
  #an internal utility function
  def make_weightlist(self, node_coords):
   weights = dict()
   n = len(self.node_list)
   #We have to iterate over indices instead of the actual nodes
   for i, j in combinations(range(n),2):
     node1, node2 = self.node_list[i], self.node_list[j]
     if not self.G.has_edge(node1, node2):
       weights[(node1,node2)] = la.norm(node_coords[i,:]-node_coords[j,:])
   self.weights = weights




