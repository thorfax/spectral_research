import networkx as nx
from base_predictor import basePredictor
import numpy as np
import numpy.linalg as la

def shortest_path_predictor(g):
 """
 A naive link predictor
 Simply predits all links between two nodes that are distance two away
 """
 lengths = nx.shortest_path_length(g)
 edge_list = []
 for n1 in lengths:
  for n2 in lengths[n1]:
   if lengths[n1][n2] == 2:
     edge_list.append((n1, n2))
 return edge_list

def adjacency_prediction(g):
 pass

#G = nx.Graph()
#G.add_edges_from([(0,1),(2,1),(1,3),(1,4),(4,5),(3,5), (6,7)])
#G.add_edges_from([(0,1),(2,1),(2,3),(3,4)])
#print shortest_path_predictor(G)

class shortestPath(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  def computeScores(self):
    lengths = nx.shortest_path_length(self.G)
    scores = np.zeros((self.num_nodes,self.num_nodes))
    rev_dict = self.node_names_to_inds()
    for n1 in lengths:
      for n2 in lengths[n1]:
        scores[rev_dict[n1]][rev_dict[n2]] = lengths[n1][n2]
    return scores

class commonNeighbors(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  def computeScores(self):
    adj = self.get_adj()
    return -adj.dot(adj)

class preferentialAttachment(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  def computeScores(self):
    adj = self.get_adj()
    node_degrees = np.sum(adj, axis = 1)
    res = np.outer(node_degrees, node_degrees)
    return -res

#This implementation is flawed, inefficient, and should not be used anymore
class Jacard(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  #Score(x,y) is the number of common neighbors of x and y divided by the total combined number of neighbors.
  #If N(z) is the set of neighbors for node z, then score(x,y) = |N(y) intersect N(x)| / |N(y) union N(x)|
  def computeScores(self):
    adj = self.get_adj()
    num = adj.dot(adj)
    adj = adj.astype(int)
    denom = np.zeros(adj.shape)
    for i in xrange(self.num_nodes):
      denom[i,:]  = np.sum(np.bitwise_or(adj[i,:], adj), axis = 1)
    return - num / denom

class AdamicAdar(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  #Score(x,y) is the number of common neighbors of x and y divided by the total combined number of neighbors.
  #If N(z) is the set of neighbors for node z, then score(x,y) = sum 1/log(|N(z)|) for z in {N(y) intersect N(x)}
  def computeScores(self):
    scores = self.empty_like()
    preds = nx.adamic_adar_index(self.G)
    rev_dict = self.node_names_to_inds()
    for u,v, score in preds:
      scores[rev_dict[u], rev_dict[v]] = scores[rev_dict[v], rev_dict[u]] = score
    return -scores



class resourceAllocation(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  def computeScores(self):
    scores = self.empty_like()
    preds = nx.resource_allocation_index(self.G)
    rev_dict = self.node_names_to_inds()
    for u,v, score in preds:
      scores[rev_dict[u], rev_dict[v]] = scores[rev_dict[v], rev_dict[u]] = score
    return scores
    

class katz(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)
    self.beta = .01

  def setBeta(self, new_beta):
    self.beta = new_beta

  def computeScores(self):
    adj = self.get_adj()
    res = la.inv(np.eye(self.num_nodes) - self.beta * adj) - np.eye(self.num_nodes)
    return -res

class pageRank(basePredictor): 
  def __init__(self, g):
    basePredictor.__init__(self, g)
    self.restart_prob = .1

  def setRestartProb(self, new_prob):
    self.restart_prob = new_prob

  def computeScores(self):
    adj = self.get_adj()
    P = adj / np.sum(adj, axis = 1)
    res = la.inv(np.eye(self.num_nodes) - self.restart_prob * P)
    return -res

class fastDegreePredictor(basePredictor):
    def __init__(self, G):
        basePredictor.__init__(self, G)
    
    def score_kernel(self, deg1, deg2):
        raise NotImplementedError("Method not implemented in base class")
    
    def predict(self, min_links=None):
        if min_links is None: min_links = self.num_nodes
        degrees = np.zeros(self.num_nodes)
        for i in xrange(self.num_nodes):
            degrees[i] = self.G.degree(self.node_list[i])

        inds = np.argsort(degrees)[::-1]
        #Need to find the best min_links pairs
        l = []
        ind_to_node = lambda i: self.node_list[inds[i]]
        for i in xrange(self.num_nodes):
            for j in xrange(i+1, min(self.num_nodes, (2*(min_links+self.num_edges) +i*(i+1)) / (2*(i+1)) + 1)):
                x,y  = ind_to_node(i), ind_to_node(j)
                if not self.G.has_edge(x,y):
                    l.append((inds[i],inds[j]))        
        l.sort(key=lambda t: self.score_kernel(degrees[t[0]], degrees[t[1]]) )
        return map(lambda t: map(lambda x: self.node_list[x], t), l[:min_links])

class InverseDegree(basePredictor):
    pass        

class fastInverseDegree(fastDegreePredictor):
    def __init__(self, G):
        fastDegreePredictor.__init__(self, G)
    
    def score_kernel(self, deg1, deg2):
        return 1./deg1 + 1./deg2

class fastPreferentialAttachment(fastDegreePredictor):
    def __init__(self, G):
        fastDegreePredictor.__init__(self, G)
    
    def score_kernel(self, deg1, deg2):
        return - deg1 * deg2
    
    def fpredict(self, min_links=None):
        if min_links is None: min_links = self.num_nodes
        degrees = np.zeros(self.num_nodes)
        for i in xrange(self.num_nodes):
            degrees[i] = self.G.degree(self.node_list[i])

        inds = np.argsort(degrees)[::-1]
        #Need to find the best min_links pairs
        l = []
        ind_to_node = lambda i: self.node_list[inds[i]]
        for i in xrange(self.num_nodes):
            for j in xrange(i+1, min(self.num_nodes, (2*(min_links+self.num_edges) +i*(i+1)) / (2*(i+1)) + 1)):
                x,y  = ind_to_node(i), ind_to_node(j)
                if not self.G.has_edge(x,y):
                    l.append((inds[i],inds[j]))        
        l.sort(key=lambda t: -degrees[t[0]] * degrees[t[1]])
        return map(lambda t: map(lambda x: self.node_list[x], t), l[:min_links])

from collections import Counter
from scipy.sparse import find
import heapq
class fastCommonNeighbors(basePredictor):
    def __init__(self, G):
        basePredictor.__init__(self, G)
    
    def normal_predict(self,min_links):
        A = nx.adjacency_matrix(self.G)
        rows, cols, vals = find(A.dot(A))
        nvalid = 0
        l = []
        for v,c,r in sorted(zip(vals, rows, cols))[::-1]:
            x,y = self.node_list[c], self.node_list[r]
            if c >= r or self.G.has_edge(x,y): continue
            else:
                l.append((x,y))
                nvalid += 1
                if nvalid == min_links: break
        return l
    
    def low_memory_predict(self, min_links):
        h = []        
        for x in self.G:
            c = Counter()
            for y in self.G[x]:
                for z in self.G[y]:
                    if z <= x or z in self.G[x]: continue
                    c[(x,z)] += 1       
            
            for el, count in c.most_common():
                if len(h) >= min_links:
                    if count > h[0][0]:                        
                        heapq.heapreplace(h, (count, el))
                else:
                    heapq.heappush(h, (count, el))
        l = sorted(h)[::-1]
        #Get the actual edges
        return [t[1] for t in l]
            
    def predict(self, min_links=None, algorithm = "normal"):
        if min_links is None: min_links = self.num_nodes
        #Do a full scan that doesn't allocate obscene amounts of memory but will be slow
        if algorithm == "low_memory": return self.low_memory_predict(min_links)
        else: return self.normal_predict(min_links)        

class jaccard(basePredictor):
  def __init__(self, g):
    basePredictor.__init__(self, g)

  #Score(x,y) is the number of common neighbors of x and y divided by the total combined number of neighbors.
  #If N(z) is the set of neighbors for node z, then score(x,y) = |N(y) intersect N(x)| / |N(y) union N(x)|
  def predict(self, min_links=None):
        if min_links is None: min_links = self.num_nodes
        A = nx.adjacency_matrix(self.G)
        rows, cols, vals = find(A.dot(A))
        nvalid = 0
        l = []
        for i in xrange(len(vals)):
            r,c = rows[i], cols[i]
            #Given the number of common neighbors and the neighbors for each node, we can compute the union's size easily
            card_union = (self.G.degree(self.node_list[r])+self.G.degree(self.node_list[c]) - vals[i])
            vals[i] /= card_union
            
        for v,c,r in sorted(zip(vals, rows, cols))[::-1]:
            x,y = self.node_list[c], self.node_list[r]
            if c >= r or self.G.has_edge(x,y): continue
            else:
                l.append((x,y))
                nvalid += 1
                if nvalid == min_links: break
        return l

  def computeScores(self):
    res = self.empty_like()
    preds = nx.jaccard_coefficient(self.G)
    for u,v, score in preds:
      res[u,v] = score
      res[v,u] = score
    return - res            
#G = nx.Graph()
#G.add_edges_from([(0,1),(1,2),(2,3),(3,4),(2,0), (0,3)])
#j = jaccard(G)
#print j.predict()
#fc = fastCommonNeighbors(G)
#print fc.predict(min_links=4, algorithm="low_memory")
#cn = commonNeighbors(G)
#print cn.predict(min_links=4)
#print fp.predict(min_links=4)
#sp = preferentialAttachment(G)
#print sp.predict(min_links=1)
#a = AdamicAdar(G)
#print a.predict()
#r = resourceAllocation(G)
#print r.predict()
#p = shortestPath(G)
#p2 = preferentialAttachment(G)
#print p2.predict()
#p3 = katz(G)
#print p3.predict()
#p4 = pageRank(G)
#print p4.predict()
