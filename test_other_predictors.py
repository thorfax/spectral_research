import other_predictors as op
import graph_utils as gu
import networkx as nx

def test_graph(G, min_links):
    slow = op.preferentialAttachment(G)
    fast = op.fastPreferentialAttachment(G)
    s_links = slow.predict(min_links=min_links)
    f_links = fast.predict(min_links=min_links)
    for l in f_links:
        assert (tuple(l) in s_links) or (tuple(l[::-1]) in s_links)
    
G = nx.Graph()
G.add_edges_from([(i, i+1) for i in xrange(6)])
G = gu.random_preferential_attach(100,10)
test_graph(G, 10)
