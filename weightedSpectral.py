from base_predictor import *
from graph_utils import smallest_laplacian_eigenspaces
import numpy as np
import networkx as nx
from spectralPredictor import *

class weightedSpectral(basePredictor):
  def __init__(self, G):
    basePredictor.__init__(self, G)
    #By default use only 
    self.num_spaces = self.num_nodes
  
  def setParam(self, param):
    param = int(param)
    if param <= 0 or param >= self.num_nodes: return
    self.num_spaces = param
  
  def computeScores(self):
   laplacian = self.get_laplacian()
   evecs, evals = smallest_laplacian_eigenspaces(laplacian, num_spaces = self.num_spaces, return_evals=True)
   evecs /= evals
   n = self.num_nodes
   weights = evecs.dot(evecs.T)
   d = np.diag(weights)
   scores = d + d.reshape((n,1)) - 2 * weights
   return scores
   #scores[i,j] should be weights[i,i] + weights[j,j] - 2* weights [i,j]

G = nx.Graph()
G.add_edges_from([(0,1),(1,2),(2,3),(3,4),(2,0)])

p = weightedSpectral(G)
print p.predict()

p1 = spectralPredictor(G)
print p1.predict()
