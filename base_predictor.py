import networkx as nx
import numpy as np

score_inf = 1e18
#A base link predictor
class basePredictor():
  def __init__(self, G):
   """
   A basic link predictor.
   """
   self.G = G
   self.node_list = G.nodes()
   self.num_nodes = len(self.node_list)
   #We use number_of_edges instead of size to avoid the possibility of size summing weights instead of actually counting the number of edges.
   self.num_edges = G.number_of_edges()
   #Please don't modify this property.
   #Note that this blatantly assumes an un-directed graph. You're welcome
   self._max_candidates = self.num_nodes*(self.num_nodes - 1)/2 - self.num_edges
   self.write_to_cache = False
   self.read_from_cache = False
   self.cache_name = None

  def empty_like(self):
    return np.zeros((self.num_nodes, self.num_nodes))

  def get_adj(self):
    return np.array(nx.to_numpy_matrix(self.G))

  def get_laplacian(self):
    A = self.get_adj()
    return np.diag(np.sum(A, axis = 0)) - A

  def node_names_to_inds(self):
    return {self.node_list[i]: i for i in xrange(self.num_nodes)}

  def validate(self, true_graph, min_links=None, force_exact=False, return_links=False, **kwargs):
    """
    DESCRIPTION: Predicts new links and validates against true_graph.
    If min_links is None, predicts only the links with the best score.
    If min_links is given, predict at least (and probably exactly) that many links.
    If force_exact = True is given, predict exactly the top min_links links 
    If return_links is True, return the links instead of the number
    RETURNS:
    links -- the number of links predicted if return_links=False, otherwise the actual links
    ncorrect -- the number of current predictions
    """    
    links = self.predict(min_links = min_links, **kwargs)
    #enforce correct predicted links
    if force_exact:
        if min_links is not None:
            num_to_predict = min_links
            while len(links) < min_links:
                num_to_predict *= 2
                links = self.predict(min_links=num_to_predict, **kwargs)

        ncorrect = 0
        links = links[:min_links]
        for link in links:
            if true_graph.has_edge(link[0], link[1]):
                ncorrect += 1
    else:
        ncorrect = 0
        for link in links:
            if true_graph.has_edge(link[0], link[1]):
                ncorrect += 1        
    if return_links:
        return links, ncorrect
    else:
        return len(links), ncorrect

  def setCacheName(self, fname):
   if fname.endswith(".npz"): self.cache_name = fname
   else: self.cache_name = fname+".npz"

  def setWriteCache(self, fname):
   self.setCacheName(fname)
   self.write_to_cache = True

  def setReadCache(self, fname):
   self.setCacheName(fname)
   self.read_from_cache = True

  def computeScores(self):
   scores = np.ones((self.num_nodes, self.num_nodes))
   return scores

  #Currently only one-parameter link prediction methods are supported
  def setParam(self, param):
    pass

  def predict(self, min_links = None, score_tolerance = 1e-10):
   """
   DESCRIPTION: predict new links in a graph
   PARAMS:
   num_nodes -- The minimum number of nodes to predict. If set to None, all candidate edges with score equal to the minimum score will be returned.
   score_tolerance -- The tolerance to be used in comparing two scores (as differences between otherwise theoretically 'equal' links will occur due to numerical error)
   RETURNS:
   edge_list -- a list of tuples [(node1, node2) of the candidate ]
   """

   if min_links is not None and min_links > self._max_candidates:
     #Trying to predict more new links than can possibly occur
     min_links = self._max_candidates
     print min_links
#     raise RuntimeError("Tried to predict %d new links in a graph with only %d non-loop edges in the complement." % (min_links, self._max_candidates) )

   #This method will usually be overridden by a sub-class. It contains most of the algorithm-specific information.
   if self.read_from_cache:
    try:
     score_arch = np.load(self.cache_name)
     scores = score_arch['scores']

     if not score_arch['scores'].size == self.num_nodes**2:
      scores = self.computeScores()

     score_arch.close()
     print "Read from cache"
    except IOError:
      scores = self.computeScores()

   else:
    scores = self.computeScores()

   if self.write_to_cache:
     np.savez(self.cache_name,scores = scores)
     print "Wrote to cache"

   A = np.array(nx.to_numpy_matrix(self.G))
   #Don't care about the scores between already-connected nodes
   scores[np.triu_indices(scores.shape[0])] = score_inf
   scores[(A > 0)] = score_inf
   np.fill_diagonal(scores, score_inf)
   min_score = np.min(scores)
   if min_links is not None:
     flat_scores = scores.flatten()
     sorted_inds = np.argsort(flat_scores)[:min_links]
     #get all scores close to the last
     last_score = flat_scores[sorted_inds[-1]]
     dest = np.zeros(flat_scores.size)
     dest[:] = flat_scores[:]
     other_inds = np.argwhere(np.abs(dest - last_score) < score_tolerance)
#     print len(other_inds), min_links
     all_inds = np.concatenate((sorted_inds[:min_links], other_inds.flatten()))
     to, frm = np.unravel_index(all_inds, dims = A.shape)
   else:
     flat_scores = scores.flatten()
     dest = np.zeros(flat_scores.size)
     dest[:] = flat_scores[:]
     all_inds = np.argwhere(np.abs(dest - min_score) < score_tolerance)
#     print all_inds.flatten()
#     print np.unravel_index(all_inds, dims = A.size)
     to, frm = np.unravel_index(all_inds.flatten(), dims = A.shape)
   return set([(self.node_list[i],self.node_list[j]) for i,j in zip(to, frm)])


if __name__=="__main__":
 G = nx.Graph()
 G.add_edges_from([(0,1), (1,2), (2,3)])
 p = basePredictor(G)
 print p.predict(min_links = 3)
