import numpy as np
import json
import networkx as nx
import argparse

parser = argparse.ArgumentParser(description = "Run experiments on the dblp dataset")
parser.add_argument("in_json", type = argparse.FileType("r"))

args = parser.parse_args()
data = json.load(args.in_json)
print "Loaded json."
years = sorted(data["year_cliques"].keys())
print years

def add_year_to_graph(G, year):
  for clique in data["year_cliques"][year]:
    for i, el1 in enumerate(clique):
      for el2 in clique[:i]:
	G.add_edge(el1, el2)

def make_graph(startYear, stopYear):
    G = nx.Graph()
    for year in years:
      if year < stopYear and year >= startYear:
	print year
	add_year_to_graph(G, year)
    return G

G = make_graph('1900','2017')	
print len(G.edges()), len(G.nodes())
 