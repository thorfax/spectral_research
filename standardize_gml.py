import json
import argparse
import graph_utils as gu
from networkx.readwrite import json_graph

p = argparse.ArgumentParser(description='convert gml graphs to my standardized json format')
p.add_argument('infile', type=str)
p.add_argument('outfile', type=argparse.FileType('w'))
args = p.parse_args()

g, labels = gu.gml_with_labels(args.infile)
json.dump({'graph':json_graph.adjacency_data(g), 'labels':labels}, args.outfile)

