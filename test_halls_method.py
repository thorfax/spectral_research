import numpy as np
import scipy.linalg as la
import networkx as nx
from itertools import combinations
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from graph_utils import *



def hall_graph(laplacian, tol = 1e-14, verbose = False):
 """
 Attempts to reconstruct the graph with laplacian L based off hall's method.
 RETURNS:
 res -- the constructed adjacency matrix
 """
 n = laplacian.shape[0]
 node_coords = fiedler_eigenspace(laplacian)
 
 res = np.zeros((n,n))
 for node1, node2 in combinations(range(n), 2):
   if la.norm(node_coords[node1,:]-node_coords[node2,:]) < feidler_eval:
    res[node1, node2] = 1
    res[node2, node1] = 1
 
 if verbose:
  print "Feidler eigenvalue:" +str(feidler_eval)
  print "Number of fiedler eigenvectors: " + str(node_coords.shape[1])
  print "Number of nodes: " + str(node_coords.shape[0])
 
 return res

def minimum_distance_pred(laplacian, tol =1e-14):
 """
 INPUTS:
 L -- the laplacian of a graph
 RETURNS:
 edge_list -- the list of predicted new edges. Edges are represented as tuples of integers (zero-based idexing)

 DESCRIPTION:
 maps vertices to points in the fiedler eigenspace
 then takes the euclidean distances between pairs of vertices corresponding to edges not in the graph
 the vertex pair(s) with the smallest distances are the new predicted edges
 """
 node_coords = fiedler_eigenspace(laplacian)
 candidate_edges = dict()
 for node1, node2 in combinations(range(n),2):
  if laplacian[node1,node2] == 0:
   candidate_edges[(node1,node2)] = la.norm(node_coords[node1,:]-node_coords[node2,:])
 min_distance = min(candidate_edges.values())
 edge_list = []
 for edge in candidate_edges:
  if abs(candidate_edges[edge]-min_distance) < tol:
   edge_list.append(edge)
 return edge_list

    
   
    
def check_hall(A,verbose=False):
 HG = hall_graph(laplacian_from_adj(A),verbose=verbose)
 return np.allclose(HG,A)

def check_graphs_in_atlas(atlas = None):
 if atlas==None:
  atlas = nx.graph_atlas_g()
 for g in atlas:
  if len(g.nodes())>1 and nx.is_connected(g):
   print check_hall(nx.adjacency_matrix(g), verbose=True)

def explore_star_graphs(n):
 for i in xrange(2,n):
  if check_hall(star_adj(i),True):
    print "Hall's method works for the star graph with %d nodes" % i
  else:
    print "Hall's method fails for the star graph with %d nodes" % i  

def draw_G_and_hall(G):
 nx.draw(G)
 plt.show()
 nx.draw(nx.from_numpy_matrix(hall_graph(nx.laplacian_matrix(G))))
 plt.show()



def test_complete_graphs(n,pfactor = 0):
 for i in xrange(2,n+1):
  G = nx.complete_graph(i)
#  for i in xrange(int(pfactor*i)):
 #  if G.has_edge(
  G.remove_edge(0,1)
  draw_G_and_hall(G)
#  print i, check_hall(nx.adjacency_matrix(G),verbose=True)

if __name__ == "__main__":
 G = nx.Graph()
 G.add_edges_from([(0,1),(0,2),(0,3),(3,4)])
 draw_spectral(nx.laplacian_matrix(G).todense())
# check_graphs_in_atlas()
 #explore_star_graphs(150)
# test_complete_graphs(10)
# for g in 


