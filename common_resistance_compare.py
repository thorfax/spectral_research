import other_predictors as op
import graph_utils as gu
from spectralPredictor import resistancePredictor
import networkx as nx

def compare(g, first=op.commonNeighbors,second=resistancePredictor,min_links=1):
        common = first(g)
        rpred = second(g)
        c_links = common.predict(min_links=min_links)
        r_links = rpred.predict(min_links=min_links)
        return float(len(r_links.intersection(c_links)))/len(r_links)

def pref_attach_expirement(nreps=100, num_nodes=100, edges_per_node=3, **kwargs):
    tot = 0.
    for i in xrange(nreps):
        g = gu.random_preferential_attach(num_nodes, edges_per_node)
        tot += 1 if compare(g, **kwargs) == 1 else 0
    print tot/float(nreps)

if __name__ == "__main__":
    #print compare(nx.karate_club_graph(), first=op.preferentialAttachment, min_links=10)
    pref_attach_expirement(first=op.katz,min_links=2)
    pref_attach_expirement(first=op.commonNeighbors, min_links=2)
