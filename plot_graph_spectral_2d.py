from link_predictor import linkPredictor
import networkx as nx
import matplotlib.pyplot as plt

def draw_elist(elist):
 g = nx.Graph()
 g.add_edges_from(elist) #the fish graph
 l = linkPredictor(g)
 l.draw_spectral(num_spaces = 1)
 l.draw_2d(d1=True)
 l.draw_nx()

fish = [(1,2), (1,3), (2,4), (3,4), (4,5), (4, 6)]
star = [(0,1), (0,2), (0,3)]
#draw_elist(fish)
foo = [(0,1), (1,2), (2,3), (3,0), (3,1)]
draw_elist(foo)


