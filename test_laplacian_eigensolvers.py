import numpy as np
import networkx as nx
import graph_utils as gu
import numpy.linalg as la
import time
import matplotlib.pyplot as plt
from eigensolvers import *

def get_l(g):
    c = gu.biggest_component(g)
    print nx.is_connected(c)
    return nx.laplacian_matrix(c)


def compare_tmin():
    tmin, spy = [],[]
    ns = [2**i for i in xrange(3,10)]
    for i in xrange(3,12):
        n = 2**i
        g = gu.random_preferential_attach(n,4)
        sparse_l = get_l(g)
        spy.append(time_solver(sparse_l, scipy_eigh))
        tmin.append(time_solver(sparse_l, tracemin_solver))

    plt.plot(spy, label="Scipy Lancoz iteration")
    plt.plot(tmin, label="Tracemin with pcg")
    plt.legend(loc='upper left')
    plt.show()

def test_cond_mat():
    g = gu.hack_gml_read("cond-mat-2003.gml")
    sparse_l = get_l(g)
    print "Tracemin, ", time_solver(sparse_l, tracemin_solver, dim=4)
    #print "Lanczos, ", time_solver(sparse_l, scipy_eigh, dim=1)

test_cond_mat()
