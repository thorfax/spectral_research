import networkx as nx
import sys
import matplotlib.pyplot as plt
from networkx.algorithms import bipartite
import json
infile = open(sys.argv[1], "r")
outfile = open(sys.argv[2], "w")
reading = False
G = nx.Graph()
graphs = []
for line in infile:
 s = line.strip()
 if s.startswith("RowBox"):
   if "UndirectedEdge" in s:
     reading = True
     a = s.split()
     e1, e2 = int(a[0][9:-2]), int(a[2][1:-4].strip("\"}"))
     G.add_edge(e1,e2)
   else:
     if len(G.edges()):
      reading = False
      s1, s2 = bipartite.sets(G)
#      print [len(G[n]) for n in s1]
#      print [len(s) for s in bipartite.sets(G)], len(G.edges())
      nnodes = len(G.nodes())
      nedges = 10*(nnodes-20)
      graphs.append(G.edges())
#      print [len(G[node])/float(nedges) for node in s2]
#      plt.hist([len(G[node]) for node in s2], bins=10)
#      plt.title("Graph on %d, %d" % (len(s1), len(s2)))
#      plt.show()
      G = nx.Graph()
json.dump(graphs, outfile)
