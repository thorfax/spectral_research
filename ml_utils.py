import numpy as np
import networkx as nx
import random
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt

def balance_classes(positives, negatives, p = .5):
    """
    ensure that the proportion of positives and negatives are correct
    p -- the proportion of classes that are positive

    return samples, labels
    """
    #maximum number of samples for the smallest class
    max_class_samples = min(len(positives), len(negatives))
    npositives = int(p * max_class_samples)
    nnegatives = int((1-p)*max_class_samples)
    random.shuffle(positives)
    random.shuffle(negatives)
    labels = np.zeros(npositives + nnegatives)
    labels[:npositives] = 1
    return positives[:npositives] + negatives[:nnegatives], labels

def select_training_edges(G):
    """
    Given a networkx graph G, select a set of vertex pairs to be used for training an ML link predictor.
    The training set will include all of the edges, as well as some proportion of non-edges.
    """
    edges = G.edges()
    G_cmpl  = nx.complement(G)
    cmpl_edges = G_cmpl.edges()

    return balance_classes(edges, cmpl_edges)

def create_features(vertex_pairs, kernel_list):
    """
    Combine multiple graph kernels and a list of vertex pairs into a dataset of features.
    """
    first, second = [], []
    for pair in vertex_pairs:
        first.append(pair[0])
        second.append(pair[1])
    features = np.zeros((len(vertex_pairs), len(kernel_list)))
    for i, kernel in enumerate(kernel_list):
        features[:,i] = kernel[np.array(first, dtype=int), np.array(second, dtype=int)]
    return features

def format_train_test(train, test):
    """
    Ensure that train and test have the same vertex set and complementary edge sets.
    """
    new_train, new_test = nx.Graph(), nx.Graph()
    reverse_dict = {n:i for i,n in enumerate(train.nodes())}
    for edge in train.edges():
        n1, n2 = edge
        new_train.add_edge(reverse_dict[n1], reverse_dict[n2])

    for edge in test.edges():
        n1, n2 = edge
        if train.has_edge(n1,n2): continue
        if n1 in reverse_dict and n2 in reverse_dict:
            new_test.add_edge(reverse_dict[n1], reverse_dict[n2])

    return new_train, new_test

def select_testing_edges(train, test, p=.5):
    """
    Let train and test be graphs on the same vertex set. Suppose train and test have disjoint edge sets.
    We want to create a 'test set' of edges. Positive examples correspond to those in the test set.
    Negative examples correspond to edges not present in either the test or training set.
    """
    positives = test.edges()
    negatives = nx.complement(nx.compose(train, test)).edges()
    return balance_classes(positives, negatives,p)

def normalize(arr):
    minimum = np.min(arr)
    denom = np.max(arr)-minimum
    if denom < 1e-10:
        return arr + minimum
    else: return (arr + np.min(arr)) / denom

def plot_roc_curve(labels, probs):    
    fpr, tpr, thresh = roc_curve(labels, probs)
    plt.plot(fpr, tpr, label = "AUC "+str(auc(fpr, tpr)))
    plt.title("Roc Curve")
    plt.legend(loc="lower right")
    plt.show()


def single_predictor_roc(kernel, train_graph, test_graph):
    train_edges, train_labels = select_training_edges(train_graph)
    test_edges, test_labels = select_testing_edges(train_graph, test_graph)
    train_features = create_features(train_edges, [kernel])
    test_features = create_features(test_edges, [kernel])
    probs = normalize(test_features.ravel())
    plot_roc_curve(test_labels, probs)

def make_kernels(predictors):
    return [pred.computeScores() for pred in predictors]

train = nx.Graph()
train.add_edges_from([(2,3),(3,4)])

test = nx.Graph()
test.add_edges_from([(2,3),(4,5),(4,2)])

ntrain, ntest = format_train_test(train, test)
#print ntrain.edges()
#print ntest.edges()
