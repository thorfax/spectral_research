import graph_utils as gu
import argparse
import json
import networkx as nx

parser = argparse.ArgumentParser(description = "Parse csv file into json graph format.")
parser.add_argument("in_csv", type = argparse.FileType("r"))
parser.add_argument("out_json", type = argparse.FileType("w"))

args = parser.parse_args()

elist = []
for line in args.in_csv:
    elist.append( map(lambda pair :tuple(map(int, pair.split(", "))), line.strip('""}{\r\n').split('}","{')))

json.dump(elist, args.out_json)
