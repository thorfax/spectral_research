import networkx as nx
import numpy as np
import scipy.linalg as la
from base_predictor import basePredictor
from graph_utils import smallest_laplacian_eigenspaces
from link_predictor import linkPredictor

class spectralPredictor(basePredictor):
  def __init__(self, G):
   basePredictor.__init__(self, G)
   #By default use only the fiedler eigenspace
   self.numEigenSpaces = 1
     
  def setNumSpaces(self, spaces):
   self.numEigenSpaces = spaces

  def computeScores(self):
   laplacian = self.get_laplacian()
   node_coordinates = smallest_laplacian_eigenspaces(laplacian, num_spaces = self.numEigenSpaces)
   n = self.num_nodes
   scores = np.zeros((n, n))

   #The scores[i,j] needs to be the euclidean distance (or squared euclidean distance) between row i and row j
   #We want to compute this using vectorized operations (so no double Python for loops) 
   ndims = node_coordinates.shape[1]
   for i in xrange(ndims):
     scores += (node_coordinates[:,i].reshape((n,1)) - node_coordinates[:,i].reshape((1,n))) ** 2
   return np.sqrt(scores)

class resistancePredictor(basePredictor):
  def __init__(self, G, rank = None):
    basePredictor.__init__(self, G)
    if rank is None or rank >= self.num_nodes or rank <= 0:
      self.rank = self.num_nodes-1 #to account for the first zero eigen-value, full rank is n-1
    else:
      self.rank = rank
  
  def computeScores(self):
    L = self.get_laplacian()
  #  evals, evecs = la.eigh(L)
 #   V = evecs[:,1:self.rank+1]
#    Ld = V.dot(np.diag(1./evals[1:self.rank+1])).dot(V.T)

    Ld = la.pinv(L)
    d = np.diag(Ld)    
    return d + d.reshape((len(d),1)) - 2*Ld

#  def predict(self):
#    return []

def resistanceMat(edge_list, rank=None):
 g = nx.Graph()
 g.add_edges_from(edge_list)
 p= resistancePredictor(g, rank=rank)
 return p.computeScores()

#l1 = [(1,3),(2,4),(3,5),(3,6),(4,5),(4,6)]
#r1 = resistanceMat(l1, rank=1)
#g = nx.Graph()
#g.add_edges_from(l1)
#sp = spectralPredictor(g)
#r2= sp.computeScores()**2
#print r1[0,1:]/r2[0,1:]
#r2 = resistanceMat([(1,3),(2,4),(3,5),(3,6),(4,5),(4,6),(6,5)])
#print r1*4
#print r2*4
