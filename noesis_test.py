
import sys
import argparse
import networkx as nx
import other_predictors as op
from spectralPredictor import spectralPredictor
import ml_utils as ml
from graph_utils import general_graph_read
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import log_loss, accuracy_score
import numpy as np

def safe_pajek_read(path):
    return nx.Graph(nx.read_pajek(path))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Explore .net files for link prediction.')
    parser.add_argument('network_name', type = str)
    parser.add_argument('number', type = int)

    args = parser.parse_args()
    pref = "data/"+args.network_name
    train_graph = safe_pajek_read(pref+"_train_"+str(args.number)+".net")
    test_graph = safe_pajek_read(pref+"_test_"+str(args.number)+".net")

#    common = other_predictors.commonNeighbors(train_graph)
#    common = other_predictors.jaccard(train_graph)
#    indegrees =
#    ml.single_predictor_roc(-op.jaccard(train_graph).computeScores(), train_graph, test_graph)
    train_edges, train_labels = ml.select_training_edges(train_graph)
    test_edges, test_labels = ml.select_testing_edges(train_graph, test_graph)

    spec = spectralPredictor(train_graph)
    spec.setNumSpaces(5)

    train_indegrees = np.array([len(train_graph[node1]) for node1, node2 in train_edges])
    test_indegrees = np.array([len(train_graph[node1]) for node1, node2 in test_edges])
    train_outdegrees = np.array([len(train_graph[node2]) for node1, node2 in train_edges])
    test_outdegrees = np.array([len(train_graph[node2]) for node1, node2 in test_edges])
    train_features = np.vstack([train_indegrees, train_outdegrees]).T
#    print train_labels, train_features

    kernels = ml.make_kernels([spec, op.katz(train_graph), op.preferentialAttachment(train_graph)])
    testf = ml.create_features(test_edges, kernels)
    trainf = ml.create_features(train_edges, kernels)

    test_features = np.vstack([test_indegrees, test_outdegrees]).T
    test_features = np.hstack([testf, test_features])
    train_features = np.hstack([trainf, train_features])
    model = RandomForestClassifier(n_estimators = 10, n_jobs = -1, max_depth = 6)
    model.fit(train_features, train_labels)
    probs = model.predict_proba(test_features)[:,1]
    ml.plot_roc_curve(test_labels, probs)

        # pred = other_predictors.commonNeighbors(train_graph)
    # pred2 = spectralPredictor(train_graph)
    # pred2.setNumSpaces(5)
    # kernels = ml.make_kernels([pred, pred2, other_predictors.katz(train_graph)])
    # train_features = ml.create_features(train_edges, kernels)
    # test_features = ml.create_features(test_edges, kernels)
    # model = RandomForestClassifier(n_estimators = 5, n_jobs = -1, max_depth = 5)
    # pred = model.predict(test_features)
    # print accuracy_score(test_labels, pred)
#    print log_loss(test_labels, probs)
#    print train_features, train_labels
    #pred.setNumSpaces(5)
