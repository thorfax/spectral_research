from base_predictor import basePredictor
import eigensolvers as es
import graph_utils as gu
import networkx as nx
import numpy as np
from closest_points import closest_points

class SpectralEmbedding(basePredictor):
    """A class to do link prediction with a laplacian spectral embedding
    This turns out to be equivalent to doing link prediction with low-rank approximations of effective resistance
    """
    def __init__(self, G, dim=1, esolver='tracemin', **kwargs):
        """
        G had better be connected, and dim needs to be smaller than the size of L, preferably a lot smaller
        """
        basePredictor.__init__(self, G)
        self.dim = dim
        self._compute_embedding(esolver, **kwargs)

    def _compute_embedding(self, esolver='tracemin', **kwargs):
        solver = None
        if esolver == 'tracemin':
            solver = es.tracemin_solver
        else:
            solver = es.scipy_eigh
#        print solver
        sig, V = solver(nx.laplacian_matrix(self.G), dim=self.dim, **kwargs)
 #       print V, sig
        self.points = V/np.sqrt(sig)
    
    def euclidean(self, min_links, dim=None):    
        pairs, dists = closest_points(self.points[:,:dim], k=min_links)
        res = []
        for i in xrange(min_links):
            u, v = self.node_list[pairs[i][0]],self.node_list[pairs[i][1]]
            if not self.G.has_edge(u,v):
                res.append((u,v))
        return res
    
    def cosine(self, min_links):
        norms = np.sqrt(np.sum(self.points**2, axis=1))
        #Need to reshape the norms array so broadcasting will work        
        pairs, dists = closest_points(self.points / norms[:,None], k= min_links)
        res = []
        for i in xrange(min_links):
            u, v = self.node_list[pairs[i][0]],self.node_list[pairs[i][1]]
            if not self.G.has_edge(u,v):
                res.append((u,v))
        return res
    
    def parse_dim(self, dim):
        if dim is None or dim <= 0 or dim > self.dim: return self.dim
       
    def predict(self, min_links=None, score_tolerance = 1e-10, method="euclidean", dim=None):
        dim = self.parse_dim(dim)
        if min_links is None:
            min_links = self.num_nodes
        if method == "euclidean":
            return self.euclidean(min_links, dim=dim)
        elif method == "cosine":
            return self.cosine(min_links)
     
    def validate(self, true_graph, min_links=None, **kwargs):
        return basePredictor.validate(self, true_graph, min_links, force_exact=True, **kwargs)
     
class cosinePredictor(SpectralEmbedding):
    def __init__(self, G, dim=1, esolver='tracemin', **kwargs):
        SpectralEmbedding.__init__(self, G, dim=dim, esolver=esolver, **kwargs)
    
    def predict(self, min_links=None, dim=None):
        dim = self.parse_dim(dim)
        if min_links is None: min_links = self.num_nodes
        norms = np.sqrt(np.sum(self.points**2, axis=1))
        #Need to reshape the norms array so broadcasting will work
        inputs = self.points / norms[:,None]        
        pairs, dists = closest_points(inputs[:,:dim], k= min_links)
        res = []
        for i in xrange(min_links):
            u, v = self.node_list[pairs[i][0]],self.node_list[pairs[i][1]]
            if not self.G.has_edge(u,v):
                res.append((u,v))
        return res
        
    
    def dumb_predict(self, min_links=None, score_tolerance=1e-10):
        return basePredictor.predict(self, min_links=min_links, score_tolerance = score_tolerance)
    
    def computeScores(self):
        #normalize the points before taking dot products
        norms = np.sqrt(np.sum(self.points**2, axis=1))
        #Need to reshape the norms array so broadcasting will work
        normed_points = self.points / norms[:,None]
        #Compute the matrix of cosine similarities
        return -np.dot(normed_points, normed_points.T)
        
g = gu.random_preferential_attach(10,4)

class CorrectedCommute(SpectralEmbedding):
    """
    A class to compute a spectral embedding for the corrected commute time kernel
    (Algorithms and Models for Network Data and Link Analysis Fouss, Saerens, Shimbo)
    """

    def __init__(self, G, dim=1, esolver = 'tracemin', **kwargs):
        SpectralEmbedding.__init__(self, G, dim, esolver, **kwargs)

    def _compute_embedding(self, esolver='tracemin', **kwargs):
        solver = None
        if esolver == 'tracemin':
            solver = es.tracemin_solver
        else:
            solver = es.scipy_eigh
        sig, V = solver(nx.laplacian_matrix(self.G), dim=self.dim, **kwargs)
 #       print V, sig
        self.points = V/np.sqrt(sig)

#g = gu.random_preferential_attach(10,4)
#g = nx.cycle_graph(4)
#print g.edges()
#p = cosinePredictor(g, dim=2)
#help(cosinePredictor)
#p = SpectralEmbedding(g, dim=3)
#s = p.computeScores()
#ls = p.predict(min_links=6)

