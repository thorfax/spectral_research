import matplotlib.pyplot as plt
x=[1,2,3,4,5]
y=[1,2,3,4,5]
plt.plot(x,y)
plt.xticks(rotation='vertical')

fig = plt.gcf()
fig.subplots_adjust(bottom=0.2)
plt.show()
