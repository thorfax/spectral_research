import dataset_extraction as de
from spectral_embedding import SpectralEmbedding
from other_predictors import fastInverseDegree
import gc
#A file to debunk lost-in-space problems with effective resistance for the graphs
#in our experiments

def do_test(testcase, min_links=1000):
    inv_deg = fastInverseDegree(testcase.train)
    se = SpectralEmbedding(testcase.train, dim=1)
    inv_links, inv_correct = inv_deg.validate(testcase.test, force_exact=True, return_links=True, min_links=1000)
    se_links, se_correct = se.validate(testcase.test, return_links=True, min_links=1000)
    print se_links

if __name__ == "__main__":
    for name in de.network_names:
        print name
        if "hep" in name:
            big, small = de.extract_network(name)
            del big
            del small
        else:
            test_case = de.extract_network(name)
            do_test(test_case)
            del test_case
        gc.collect()
