from link_predictor import linkPredictor
import networkx as nx
import matplotlib.pyplot as plt

g = nx.Graph()
g.add_edges_from([(1,2), (1,3), (2,4), (3,4), (4,5), (4, 6)]) #the fish graph
l = linkPredictor(g)
l.draw_2d()


#This program initializes a graph, then runs the link predictor algorithm with different numbers of eigenvectors
#After each
if __name__ == "__main__":
 g = nx.Graph()
 elist=[(1,2), (2,3), (3,4), (4,5),(5,2),(6,4),(6,2)]
# g.add_edges_from([(1,2), (1,3), (2,4), (3,4), (4,5), (4, 6)]) the fish graph
 g.add_edges_from(elist)
 l = linkPredictor(g)
 l.draw_nx()
 for i in xrange(1,6):
  g.clear()
  g.add_edges_from(elist)
  #The keyword force-dimension will forcibly map the graph to R^i, projecting down (using the SVD) if needed.
  #If repeated eigen-values are present, and projection occurs, there will be random results (due to repeated singular values).
  #I hope to find a projection method that avoids repeated singular values.
  #An alternate keyword is num_spaces, which sets the number of eigenspaces to be used.
  #This avoids the randomness inherent in SVD-projection with repeated eigen-values, but doesn't directly control the dimension
  #For link prediction, num_spaces shoul be used
  links = l.predictLinks(return_weights = False,force_dimension=i)
  print "Predicted Links with "+str(i)+" dimensions", links
  #add the predicted edges
  g.add_edges_from(links)
  plt.title("Using %d dimensions"%i)
  l.draw_nx() #this will sometimes produce wierd drawings. I am currently working on finding better visualization techniques
  
  #Forces the dimension for drawing to be 2. I still need labels for this graph.
  l.draw_spectral(force_dimension = 2)

